package com.oncore.grpc.service;

import org.lognet.springboot.grpc.GRpcService;

import com.oncore.grpc.proto.GreetingGrpc.GreetingImplBase;
import com.oncore.grpc.proto.Hello.HelloRequest;
import com.oncore.grpc.proto.Hello.HelloResponse;
 
import io.grpc.stub.StreamObserver;


@GRpcService
public class HelloService extends GreetingImplBase{

	@Override
	public void hello(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
	 
		 String name = request.getName();
		 System.out.println("Name from request :"+name);
		 
		 responseObserver.onNext(HelloResponse.newBuilder().setReply("hello "+name).build());
		 responseObserver.onCompleted();
	}  

}
